const lightswitch = document.querySelector('.light-switch')

//              0           1
let myArray = ['Herbert', 'Max']

//           0
myArray = ['Susi']

const person1 = {
    personId: 58,
    firstName: 'Franz',
    lastName: 'Meier',
    address: {
        street: 'sdfffsdfds',
        zip: 1160,
        city: 'Wien'
    }
}

console.log(person1.firstName)
person1.firstName = 'Hans'
person1.email = 'hans@meier.at'
console.log(person1)
console.log(person1.address.street)

const key = 'firstName'
console.log(person1[key])


if (lightswitch) {
    /*function onLightswitchClick() {
        console.log("Hallo Welt")
    }
    lightswitch.addEventListener('click', onLightswitchClick)*/

    lightswitch.addEventListener('click', function () {
        document.body.classList.toggle('dark')

        if (document.body.classList.contains('dark')) {
            lightswitch.textContent = 'Set light mode'
        } else {
            lightswitch.textContent = 'Set dark mode'
        }
    })

    lightswitch.addEventListener('click', function () {
        console.log("Foo")
    })

    lightswitch.onclick = function () {
        console.log("Hallo Welt")
    }

    lightswitch.onclick = function () {
        console.log("Test")
    }
}

function printScreenSize() {
    console.log(window.innerWidth, window.innerHeight)
}

window.addEventListener('resize', printScreenSize)
printScreenSize()



const employees = [
    {
        firstName: 'Franz',
        lastName: 'Meier'
    },
    {
        firstName: 'Susi',
        lastName: 'Fischer'
    },
    {
        firstName: 'Ursula',
        lastName: 'Stein'
    }
]

function sortEmployees(key) {
    employees.sort(function (person1, person2) {
        console.log(person1, person2)
        return person1[key].localeCompare(person2[key]);
    })
}

sortEmployees('lastName')
sortEmployees('firstName')
console.log(employees)